# Twitter_Feed

This is a fork of the Drupal 8 [Twitter_Feed][d8_twitter_feed] module to remove dependency on [Libraries][d8_libraries].

At the time of writing, Libraries has no release (not even an alpha). The only use of the Libraries module here was to include a [jQuery plugin][jq_timeago] to display tweet timestamps in “…minutes ago” format, which we have no need for.

[d8_twitter_feed]: https://www.drupal.org/project/issues/twitter_feed
[d8_libraries]: https://www.drupal.org/project/issues/libraries
[jq_timeago]: https://github.com/rmm5t/jquery-timeago
