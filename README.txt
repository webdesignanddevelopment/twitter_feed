INTRODUCTION
------------

The Twitter Feed module displays a configurable list of tweets in a block.
It does so using the Twitter REST API and the jQuery Timeago plugin to
display dates in a friendly, relative format (eg. 4 hours ago).

No assumptions about CSS.
Responses are cached.
No extra dependencies. Just install and go.
Output is fully themeable.

At the moment, this module is just a sandbox.

REQUIREMENTS
------------

None.

INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. See:
   https://www.drupal.org/documentation/install/modules-themes/modules-8
   for further information.

CONFIGURATION
-------------

 * There are 2 places you have to configura Twitter Feed:

   - Administration » Configuration » Web Services » Twitter Feed:

     This is the place where the Twitter API settings will be made.

   - Block placement screen:

     Here you can customize some display settings.

MAINTAINERS
-----------

Current maintainers:
 * Felipe Fidelix (Fidelix) - https://www.drupal.org/user/786032
